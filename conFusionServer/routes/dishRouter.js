const express = require('express');
const bodyParser = require('body-parser');

const authenticate = require('../authenticate');

const Dishes = require('../model/dishes');

const dishRouter = express.Router();
dishRouter.use(bodyParser.json());

dishRouter.route('/')
.get((req, res, next)=>{
    Dishes.find({})
    .then((dishes)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(dishes);
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.post(authenticate.verifyUser, (req, res, next)=>{
    Dishes.create(req.body)
    .then((dish)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(dish);
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.put(authenticate.verifyUser, (req, res, next)=>{
    res.statusCode = 403;
    res.setHeader('Content-Type', 'text/plain');
    res.end('PUT operation not supported on ' + req.baseUrl);
})
.delete(authenticate.verifyUser, (req, res, next)=>{
    Dishes.remove({})
    .then((result)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
    }, (err)=>next(err))
    .catch((err)=>next(err));
    //res.end('Deleting all the dishes!');
});

dishRouter.route('/:dishId')
.all((req, res, next)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    next();
})
.get((req, res, next)=>{
    Dishes.findById(req.params.dishId)
    .then((dish)=>{
        res.json(dish);
    }, (err)=>next(err))
    .catch((err)=>next(err));
    //res.end('Will send details of the dish: ' + req.params.dishId + ' to you!');
})
.post(authenticate.verifyUser, (req, res, next)=>{
    res.statusCode = 403;
    res.setHeader('Content-Type', 'text/plain');
    res.end('POST operation not supported on '+ req.baseUrl + '/' + req.params.dishId);
})
.put(authenticate.verifyUser, (req, res, next)=>{
    Dishes.findByIdAndUpdate(req.params.dishId, {
        $set: req.body
    }, {new: true})
    .then((dish)=>{
        res.json(dish);
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.delete(authenticate.verifyUser, (req, res, next)=>{
    Dishes.findByIdAndRemove(req.params.dishId)
    .then((result)=>{
        res.json(result);
    }, (err)=>next(err))
    .catch((err)=>next(err));
//    res.end('Deleting dish: ' + req.params.dishId);
});


dishRouter.route('/:dishId/comments')
.all((req, res, next)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    next();
})
.get((req, res, next)=>{
    Dishes.findById(req.params.dishId)
    .then((dish)=>{
        if (dish != null)   {
            res.json(dish.comments);
        }
        else{
            err = new Error('Dish ' + req.params.dishId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.post(authenticate.verifyUser, (req, res, next)=>{
    Dishes.findById(req.params.dishId)
    .then((dish)=>{
        if(dish != null)    {
            //dish.comments.push(req.body);
            dish.comments = dish.comments.concat(req.body);
            dish.save()
            .then((dish)=>{
                res.json(dish);
            });
        }
        else{
            err = new Error('Dish ' + req.params.dishId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.put(authenticate.verifyUser, (req, res, next)=>{
    res.statusCode = 403;
    res.setHeader('Content-Type', 'text/plain');
    res.end('PUT operation not supported on ' + req.baseUrl + '/' + req.params.dishId + "/comments");
})
.delete(authenticate.verifyUser, (req, res, next)=>{
    Dishes.findById(req.params.dishId)
    .then((dish)=>{
        if(dish != null)    {
            for (var i = dish.comments.length - 1; i >= 0; i--)  {
                dish.comments.id(dish.comments[i]._id).remove();
            }
            dish.save()
            .then((result)=>{
                res.json(result);
            });
        }
        else{
            err = new Error('Dish ' + req.params.dishId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err)=>next(err))
    .catch((err)=>next(err));
});

dishRouter.route('/:dishId/comments/:commentId')
.all((req, res, next)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    next();
})
.get((req, res, next)=>{
    Dishes.findById(req.params.dishId)
    .then((dish)=>{
        if(dish != null && dish.comments.id(req.params.commentId) != null)    {
            res.json(dish.comments.id(req.params.commentId));
        }
        else if(dish == null){
            err = new Error('Dish ' + req.params.dishId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Comment ' + req.params.commentId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.post(authenticate.verifyUser, (req, res, next)=>{
    res.statusCode = 403;
    res.setHeader('Content-Type', 'text/plain');
    res.end('POST operation not supported on '+ req.baseUrl + '/' + req.params.dishId + "/comments/" + req.params.commentId);
})
.put(authenticate.verifyUser, (req, res, next)=>{
    Dishes.findById(req.params.dishId)
    .then((dish)=>{
        if(dish != null && dish.comments.id(req.params.commentId) != null)    {
            if(req.body.rating) {
                dish.comments.id(req.params.commentId).rating = req.body.rating;
            }
            if(req.body.comment){
                dish.comments.id(req.params.commentId).comment = req.body.comment;
            }
            dish.save()
            .then((dish)=>{
                res.json(dish);
            }, (err)=>next(err));
        }
        else if(dish == null){
            err = new Error('Dish ' + req.params.dishId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Comment ' + req.params.commentId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.delete(authenticate.verifyUser, (req, res, next)=>{
    Dishes.findById(req.params.dishId)
    .then((dish)=>{
        if(dish != null && dish.comments.id(req.params.commentId) != null)    {
            dish.comments.id(req.params.commentId).remove();
            dish.save()
            .then((dish)=>{
                res.json(dish);
            }, (err)=>next(err));
        }
        else if(dish == null){
            err = new Error('Dish ' + req.params.dishId + ' not found');
            err.status = 404;
            return next(err);
        }
        else {
            err = new Error('Comment ' + req.params.commentId + ' not found');
            err.status = 404;
            return next(err);
        };
    }, (err)=>next(err))
    .catch((err)=>next(err));
});

module.exports = dishRouter;