const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
const Promotions = require('../model/promotions');

promoRouter = express.Router();
promoRouter.use(bodyParser.json());

promoRouter.route('/')
.get((req, res, next)=>{
    Promotions.find({})
    .then((result)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.post(authenticate.verifyUser, (req, res, next)=>{
    Promotions.create(req.body)
    .then((promotion)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(promotion);
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.put(authenticate.verifyUser, (req, res, next)=>{
    res.statusCode = 403;
    res.end('PUT operation not supported on ' + req.baseUrl);
})
.delete(authenticate.verifyUser, (req, res, next)=>{
    Promotions.remove({})
    .then((result)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
    }, (err)=>next(err))
    .catch((err)=>next(err));
});

promoRouter.route('/:promoId')
.get((req, res, next)=>{
    Promotions.findById(req.params.promoId)
    .then((promotion)=>{
        if (promotion != null) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(promotion);
        }
        else {
            err = new Error('Promotion ' + req.params.promoId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.post(authenticate.verifyUser, (req, res, nex)=>{
    res.statusCode = 403;
    res.end('POST operation not supported on '+ req.baseUrl + '/' + req.params.promoId);
})
.put(authenticate.verifyUser, (req, res, next)=>{
    Promotions.findByIdAndUpdate(req.params.promoId,
    {$set: req.body}, {new: true})
    .then((result)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.delete(authenticate.verifyUser, (req, res, next)=>{
    Promotions.findByIdAndRemove(req.params.promoId)
    .then((result)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);    
    },(err)=>next(err))
    .catch((err)=>next(err));
});

module.exports = promoRouter;