var express = require('express');
const bodyParser = require('body-parser');
var User = require('../model/user');

var passport = require('passport');
var authenticate = require('../authenticate');

var router = express.Router();
router.use(bodyParser.json())

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');      
  res.json({});

  // User.find({})
  // .then((users)=>{
  //   res.statusCode = 200;
  //   res.setHeader('Content-Type', 'application/json');      
  //   res.json(users);
  // }, (err)=>next(err))
  // .catch((err)=>next(err));
});

router.post('/signup', function(req, res, next){
  User.register(new User({username:req.body.username}), req.body.password, (err, user)=>{
    if(err)  {
      err.status = 500;
      res.setHeader('Content-Type', 'application/json');
      res.json({err: err});
    }
    else{
      // passport.authenticate('local')(req,res,()=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json({success: true, status: 'Registration Successful'});
      // });
    }
  });
});

router.post('/login', passport.authenticate('local'), (req, res)=>{
  var token = authenticate.getToken({_id: req.user._id});
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json({success: true, token: token, status: 'Login Successful'});
});

router.get('/logout', (req, res, next)=>{
  if (req.session)  {
    req.session.destroy();
    res.clearCookie('session-id');
    res.redirect('/');
  }
  else {
    var err = new Error('You are not logged in');
    err.status = 403;
    next(err);
  };
});

router.delete('/', (req, res, next)=>{
  User.remove({})
  .then((result)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');      
    res.json(result);
  }, (err)=>next(err))
  .catch((err)=>next(err));
})

module.exports = router;
