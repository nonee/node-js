const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
const Leaders = require('../model/leaders');

leaderRouter = express.Router();

leaderRouter.use(bodyParser.json());

leaderRouter.route('/')
.get((req, res, next)=>{
    Leaders.find({})
    .then((leaders)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(leaders);
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.post(authenticate.verifyUser, (req, res, next)=>{
    Leaders.create(req.body)
    .then((result)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.put(authenticate.verifyUser, (req,res,next)=>{
    res.statusCode = 403;
    res.end('PUT operation not supported on ' + req.baseUrl);
})
.delete(authenticate.verifyUser, (req,res,next)=>{
    Leaders.remove({})
    .then((result)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
    }, (err)=>next(err))
    .catch((err)=>next(err));
});

leaderRouter.route('/:leaderId')
.get((req, res, next)=>{
    Leaders.findById(req.params.leaderId)
    .then((leader)=>{
        if (leader != null) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(leader);
        }
        else {
            err = new Error ('Leader ' + req.params.leaderId + ' not found');
            err.status = 404;
            return next(err);
        }
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.post(authenticate.verifyUser, (req, res, nex)=>{
    res.statusCode = 403;
    res.end('POST operation not supported on '+ req.baseUrl + '/' + req.params.leaderId);
})
.put(authenticate.verifyUser, (req, res, nex)=>{
    Leaders.findByIdAndUpdate(req.params.leaderId)
    .then((result)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
    }, (err)=>next(err))
    .catch((err)=>next(err));
})
.delete(authenticate.verifyUser, (req, res, nex)=>{
    Leaders.findByIdAndRemove(req.params.leaderId)
    .then((result)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
    }, (err)=>next(err))
    .catch((err)=>next(err));
});

module.exports = leaderRouter;