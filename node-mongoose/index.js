const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const Dishes = require('./dishes');

const username = 'theone';
const password = 'gkskfh123';
const url = `mongodb://${username}:${password}@192.168.3.101:27017/conFusion?authSource=admin`;

const connect = mongoose.connect(url, {
    useMongoClient: true
});

connect.then((db)=>{
    console.log("Connected correctly to server");

    Dishes.create({
        name: 'Uthappizza',
        description: 'test'
    })
    .then((dish)=>{
        console.log(dish);
        return Dishes.findByIdAndUpdate(dish._id, {
            $set: {description: 'test!'}
        },{
            new: true
        }).exec();
    })
    .then((dish)=>{
        console.log(dish);
        // error : pushAll removed
        // dish.comments.push({
        //     rating: 5,
        //     comment: 'good',
        //     author: 'no one'
        // });
        dish.comments = dish.comments.concat({
            rating: 5,
            comment: 'good',
            author: 'no one'
        });
        return dish.save();
    })
    .then((dish)=>{
        console.log(dish);
        return db.collection('dishes').drop();
    })
    .then((dish)=>{
        return db.close();
    })
    .catch((err)=>{
        console.log(err);
    });
});