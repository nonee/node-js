const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// 4.6.4 이후 버전 pushAll 관련 오류 해결방법
// new Schema({ arr: [String] }, { usePushEach: true });
const commentSchema = new Schema({
    rating:{
        type: Number,
        min: 1,
        max: 5,
        require: true
    },
    comment:{
        type: String,
        require: true
    },
    author:{
        type: String,
        require: true
    }
}, {
    timestamps:true
});

const dishSchema = new Schema({
    name: {
        type: String,
        require: true,
        unique: true
    },
    description:    {
        type: String,
        require: true
    },
    comments: [commentSchema]
}, {
    timestamps: true
});

var Dishes = mongoose.model('Dish', dishSchema);

module.exports = Dishes;