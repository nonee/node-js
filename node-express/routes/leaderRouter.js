const express = require('express');
const bodyParser = require('body-parser');

leaderRouter = express.Router();

leaderRouter.use(bodyParser.json());

leaderRouter.route('/')
.all((req,res,next)=>{
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req, res, next)=>{
    res.end('Will send all the leaders to you!');
})
.post((req, res, next)=>{
    res.end('Will add the leader: ' + req.body.name + ' with details: ' + req.body.description);
})
.put((req,res,next)=>{
    res.statusCode = 403;
    res.end('PUT operation not supported on ' + req.baseUrl);
})
.delete((req,res,next)=>{
    res.end('Deleting all the leaders!');
});

leaderRouter.route('/:leaderId')
.get((req, res, next)=>{
    res.end('Will send details of the leader: ' + req.params.leaderId + ' to you!');
})
.post((req, res, nex)=>{
    res.statusCode = 403;
    res.end('POST operation not supported on '+ req.baseUrl + '/' + req.params.leaderId);
})
.put((req, res, nex)=>{
    res.write('Updating leader: ' + req.params.leaderId + '\n');
    res.end('Will update leader: ' + req.body.name + ' with details: ' + req.body.description);
})
.delete((req, res, nex)=>{
    res.end('Deleting leader: ' + req.params.leaderId);
});

module.exports = leaderRouter;