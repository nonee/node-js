const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const dboper =require('./operations');
const username = 'theone';
const password = 'gkskfh123';
const url = `mongodb://${username}:${password}@192.168.3.101:27017/conFusion?authSource=admin`;

MongoClient.connect(url)
    .then((db) => {
        console.log("Connected correctly to server");
        return dboper.insertDocument(db, {"name":"Uthappizza", "description":"test"}, "dishes")
    .then((result) =>{
        console.log('Insert Document:\n', result.ops);
        return dboper.findDocument(db, "dishes");
    })
    .then((docs) =>{
        console.log('Found Document:\n', docs);            
        return dboper.updateDocument(db, {name:"Uthappizza"} , { description:"test!" }, "dishes");
    })
    .then((result) =>{
        console.log('Update Document:\n', result.result);
        return dboper.findDocument(db, "dishes");
    })
    .then((docs) =>{
        console.log('Found Updated Document:\n', docs);            
        return db.dropCollection("dishes");            
    })
    .then((result)=>{
        console.log('Dropped Collection:\n', result);            
        return db.close();
    })
    .catch((err)=>console.log(err));
})
.catch((err)=>console.log(err));